﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;
using System.Timers;

namespace KakaoTalk_AdsRemoved
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        static extern int GetClassNameW(int hWnd, StringBuilder lpClassName, int nMaxCount);
        [DllImport("user32")]
        public static extern int GetWindowRect(int hwnd, ref RECT lpRect);
        [DllImport("user32.dll")]
        public static extern int FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        public static extern int FindWindowEx(int hWnd1, int hWnd2, string lpsz1, string lpsz2);

        [DllImport("user32")]
        public static extern Boolean ShowWindow(int hWnd, Int32 nCmdShow);
        [DllImport("user32")]
        public static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, int wMsg, int wParam, int lParam);


        private const int WM_NCHITTEST = 0x84;
        private const int HT_CLIENT = 0x1;
        private const int HT_CAPTION = 0x2;

        private const int HWND_BOTTOM = 1;

        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_NORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_MAXIMIZE = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_SHOW = 5;
        private const int SW_MINIMIZE = 6;
        private const int SW_SHOWMINNOACTIVE = 7;
        private const int SW_SHOWNA = 8;
        private const int SW_RESTORE = 9;

        private const short SWP_NOMOVE = 0X2;
        private const short SWP_NOSIZE = 1;
        private const short SWP_NOZORDER = 0X4;
        private const int SWP_SHOWWINDOW = 0x0040;

        private const int WM_CLOSE = 0x0010;


        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }


        public Form1()
        {
            InitializeComponent();
            button1.TabStop = false;
            button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatAppearance.MouseDownBackColor = Color.Transparent;
            button1.FlatAppearance.MouseOverBackColor = Color.Transparent;
            button1.BackColor = Color.Transparent;
            button1.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            new Thread(new ThreadStart(startKakaoTalk)).Start();
            new Thread(new ThreadStart(run)).Start();
        }

        private void print(string a)
        {
            Console.WriteLine(a);
        }

        private void run()
        {
            int remove_count = 0;
            while (remove_count < 4)
            {
                int nHwnd = FindWindow(null, "카카오톡");
                string ClassName = GetWindowClass(nHwnd);
                if (ClassName == null)
                {
                    nHwnd = FindWindow(null, "카카오톡");
                    ClassName = GetWindowClass(nHwnd);
                }


                if (nHwnd != 0 && ClassName.Equals("#32770"))
                {
                    print(ClassName);
                    RECT Rect = default(RECT);
                    GetWindowRect(nHwnd, ref Rect);
                    int friendList = FindWindowEx(nHwnd, 0, "#32770", null);
                    int childAd = FindWindowEx(nHwnd, 0, "EVA_Window", null);

                    if (childAd > 0)
                    {
                        ShowWindow(childAd, SW_HIDE);
                        SetWindowPos(childAd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE);
                        SetWindowPos(friendList, HWND_BOTTOM, 0, 0, (Rect.right - Rect.left), (Rect.bottom - Rect.top - 36), SWP_NOMOVE);
                        remove_count++;
                    }

                    if (ClassName.Equals("EVA_Window") || ClassName.Equals("FAKE_WND_REACHPOP"))
                    {
                        SendMessage(nHwnd, WM_CLOSE, 0, 0);
                    }
                }
            }
            Close();
            Application.Exit();
        }

        string GetWindowClass(int hWnd)
        {
            string className = String.Empty;
            int length = 10; // deliberately small so you can 
                             // see the algorithm iterate several times. 
            StringBuilder sb = new StringBuilder(length);
            while (length < 1024)
            {
                int cchClassNameLength = GetClassNameW(hWnd, sb, length);
                if (cchClassNameLength == 0)
                {
                    return null;
                }
                else if (cchClassNameLength < length - 1) // -1 for null terminator
                {
                    className = sb.ToString();
                    break;
                }
                else length *= 2;
            }
            return className;
        }

        private void startKakaoTalk()
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = "C:\\Program Files (x86)\\Kakao\\KakaoTalk\\KakaoTalk.exe";
            using (Process proc = Process.Start(start))
            {
                proc.WaitForExit();
            }
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_NCHITTEST)
                m.Result = (IntPtr)(HT_CAPTION);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }
    }
}

![Logo](/krPlatypus/kakaotalk/raw/a0424719aad88b2d1b2ca83893d0be292a67edae/images/logo.png)
## 안내사항

### 사용시 필요한 설정
> 1. .Net Framework 4.6.1이 설치되어 있는지 확인.
> 2. KakaoTalk 실행파일이 C:\Program Files (x86)\Kakao\KakaoTalk 에 위치하는지 확인
> 3. 다운받은 exe 파일을 위 경로에 위치하고, 시작프로그램에 바로가기로 등록.

### 참고 자료
> 참조 코드 : http://real21c.com/kakao-noadv/
> .Net Framework 4.6.1 = https://www.microsoft.com/ko-kr/download/details.aspx?id=49982